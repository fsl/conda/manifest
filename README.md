# FSL release manifest


This repository is used to manage FSL releases. The `fsl-release.yml` file
defines the packages that are installed as part of a FSL release.


FSL release `manifest.json` and `environment.yml` files are automatically
generated from information stored in this repository, using CI rules and
scripts in the fsl/conda/manifest-rules> repository.


This repository contains two primary branches, `main` and `external`,
and may periodically contain development/release-candidate branches.
These branches are used to create three different types of FSL release:

 - Public FSL releases (e.g. FSL 6.0.6) may be created from **tags** on the
   `external` branch.
 - Development releases (development versions of public releases) may be
   created from **commits** on any branch.
 - Internal FSL releases (for internal use only) may be created from
   **commits** on the `main` branch


More information about FSL release management procedures is available at the
fsl/conda/docs> repository.


## The `fslinstaller.py` script


FSL is installed using a Python 2/3 script called
[fslinstaller.py](https://git.fmrib.ox.ac.uk/fsl/conda/installer).


The `fslinstaller` script starts by downloading a _manifest_ file (referred
to as `manifest.json`), a JSON file which contains information about:

  - available FSL releases (past and present)
  - conda installer releases, upon which FSL installations are based
  - The latest available version of the fslinstaller script itself (used
    for self-updating)


Once the appropriate FSL release has been determined, the `fslinstaller`
downloads a FSL release file - A FSL release is defined by a conda
environment specification file (referred to as `environment.yml`), which
is a YAML file that contains a list of the individual packages and their
respective versions that comprise a specific FSL release. Separate
`environment.yml` files exist for each supported platform.


## Generating the `manifest.json` and `environment.yml` files


The `manifest.json` and `environment.yml` files are automatically generated
from information stored in this repository:

 - The `fsl-release.yml` file contains information about the `fslinstaller`,
   miniconda installers, and packages for the latest FSL release.
 - Information about past FSL releases is obtained from the most recently
   generated `manifest.json` file.


The `fsl-release.yml` file is a YAML file with the following structure:

```yaml
# URL to FSL release information. The fslinstaller.py
# script, and previously generated manifest.json
# file is assumed to be downloadable from this URL.
# URLs for newly generated environment files are
# constructed using this URL.
release-url: https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/releases/

# URL to FSL license. The user is asked to agree to
# the license terms when the fslinstaller.py script
# is run.
license-url: https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Licence

# URL to FSL registration site. The fslinstaller.py script
# will upload some basic metrics to this site for each
# installalion.
registration-url: https://fsl.fmrib.ox.ac.uk/fsldownloads/registration/


# Latest fslinstaller version
installer: 1.2.3


# URLs to miniconda/micromamba installers
# to use for all supported platforms
miniconda:
 linux-64:
  python3.10: https://github.com/conda-forge/miniforge/releases/download/4.10.3-1/Miniforge3-4.10.3-1-Linux-x86_64.sh
  python3.11: https://github.com/conda-forge/miniforge/releases/download/4.10.3-1/Miniforge3-4.10.3-1-Linux-x86_64.sh
  micromamba: https://anaconda.org/conda-forge/micromamba/2.0.5/download/linux-64/micromamba-2.0.5-0.tar.bz2
 macos-64:
  python3.10: https://github.com/conda-forge/miniforge/releases/download/4.10.3-1/Miniforge3-4.10.3-1-MacOSX-x86_64.sh
  python3.11: https://github.com/conda-forge/miniforge/releases/download/4.10.3-1/Miniforge3-4.10.3-1-MacOSX-x86_64.sh
  micromamba: https://anaconda.org/conda-forge/micromamba/2.0.5/download/osx-64/micromamba-2.0.5-0.tar.bz2
 macos-M1:
  python3.10: https://github.com/conda-forge/miniforge/releases/download/4.10.3-1/Miniforge3-4.10.3-1-MacOSX-arm64.sh
  python3.11: https://github.com/conda-forge/miniforge/releases/download/4.10.3-1/Miniforge3-4.10.3-1-MacOSX-arm64.sh
  micromamba: https://anaconda.org/conda-forge/micromamba/2.0.5/download/osx-arm64/micromamba-2.0.5-0.tar.bz2

# Conda channels to source packages from
channels:
 - https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/
 - conda-forge

# Private channel containing packages that are
# only released internally. This is prepended
# to the channels list for internal releases.
internal-channel: http://${FSLCONDA_USERNAME}:${FSLCONDA_PASSWORD}@https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/internal/

# Versions of certain core packages are pinned
# when FSL packages are built. Their versions
# may be specified here, or in the main packages
# list below.
build-packages:
 - boost-cpp
 - zlib
 ...

# List of all packages that comprise a FSL
# installation. All packages not installed
# in the first stage (see base_packages
# above) are installed in the second stage.
packages:
 - ciftilib 1.5.3
 - fsleyes 1.3.0
 - fslpy 3.7.0
 - fsl-avwutils 2007.0
 - fsl-base 2109.1
 - fsl-eddy 2106.0
 - fsl-sub 2.5.8
 - libopenblas 0.3.18 *openmp*
 ...

# List of additional platform-specific
# packages to be installed.
linux-64-packages:
 - fsl-cudabasisfield-cuda-10.2 1.1.0
 - fsl-eddy-cuda-10.2 2205.0
macos-64-packages: []
macos-M1-packages: []

# Additional groups of packages which
# are installed as separate child
# environments into $FSLDIR/envs/
extras:
 truenet:
  - fsl-truenet
  - pytorch
```


When changes are made to the `fsl-release.yml` file, new versions of the
`manifest.json` and `environment.yml` files are generated and made available
for download.


Note that there are two types of manifest file:

  - The official release manifest, containing information about past and
    present public FSL releases - this is always called `manifest.json`, and
    may be re-generated on new public releases

  - Development manifests, containing information about one internal or
    development FSL release. Development manifests are generated for every
    internal or development release. Development manifests are named according
    to a standard convention described in the **Internal/development FSL
    releases** section, below.


Development manifest and environment files can be published from any branch,
but the official release manifest can only be generated/published from tags.


## New FSL releases


_Summary:_
 - New public FSL release are created by adding a tag to the `external` branch
   of this repository
 - The `publish-release` job, on the CI pipeline for the new tag, must be
   manually started in order to publish the new release.


New public FSL releases are denoted by adding a tag to the `external` branch
of this repository. The tag is used as the FSL version identifier, which must
be of the form `X.Y.Z[.W]`, where X, Y, Z, and W are integers.

When a new tag is added to this repository, the `manifest.json` and
`environment.yml` files are generated from the contents of `fsl-release.yml`,
and the contents of the previous `manifest.json` (used for obtaining
information about past FSL releases). These files are named according to these
conventions:

  - `manifest.json` - the release manifest, re-generated for each new release.
  - `fsl-<tag>_<platform>.yml` - FSL version `<tag>` for platform `<platform>`
    (e.g. `fsl-6.1.0_macos-64.yml`).
  - `manifest-<tag>.json` - the release manifest for this release, generated
    for archival/recovery purposes.


After they have been generated, a test installation of the new release on each
supported platform is performed using the `fslinstaller.py` script, and then
the manifest and environment files are published to the FSL release web
server.  This publishing step must be manually triggered through the GitLab
web interface, via the `publish-release` job.


## Internal/development FSL releases


_Summary:_
 - New internal FSL releases may be created by adding commits to the `main`
   branch of this repository
 - New development releases may be created by adding commits to any branch
   of this repository
 - In both cases, the `publish-release` job on the associated CI pipelines
   must be started manually.


When commits are added to any branch of this repository, separate
`manifest.json` and `environment.yml` files are generated from the contents of
`fsl-release.yml` and made available for download.  These files are assigned a
version identifier of the form `<tag>.<date>.<commit>.<branch>`, where:


where:
  - `<tag>` is the _most recent_ public FSL release version identifier (e.g
    `6.0.6`).
  - `<date>` is the date of the commit that the files are generated from,
    in the form `YYYYMMDD`.
  - `<commit>` is the short hash of the git commit that the files are
    generated from.
  - `<branchname>` is the name of the branch on this repository.


The released files are then named as follows, where:

  - `manifest-<tag>.<date>.<commit>.<branch>.json` - the development release
    manifest file
  - `fsl-<tag>.<date>.<commit>.<branch>_<platform>.yml` - FSL environment file
    for platform `<platform>`.

After they have been generated, the manifest and environment files may be
published. As with public releases, the process of publishing the manifest and
environment files must be manually triggered through the GitLab web interface,
via the `publish-release` job.
