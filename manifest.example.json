// The FSL installer manifest file contains information
// about the fslinstaller script, and about versions of
// FSL that are available for download.
//
// This file is an example. The official installer
// manifest.json file is automatically generated/updated
// by the generate_manifest_file.py script, which is run
// when a new version of FSL is released.
{

    // The "installer" section contains information about the
    // latest available version of the fslinstaller script
    "installer" : {

        // Latest version of fslinstaller script
        "version" : "1.3.4",

        // URL to download fslinstaller script
        "url" : "https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/releases/fslinstaller.py",

        // SHA256 checksum of installer script
        "sha256"  : "2626342288e232d56bd16898ab6b46a348e21bb7f06bb8371fbf7479d7b5e4e8",

        // URL to the FSL license, shown to
        // the user during installation
        "license_url" : "https://fsl.fmrib.ox.ac.uk/fsl/docs/#/license",

        // URL to a registration site - the
        // fslinstaller will upload details
        // about the installation to this site.
        "registration_url" : "https://fsl.fmrib.ox.ac.uk/fsldownloads/registration"
    },

    // URLs to download miniconda/miniforge/micromamba
    "miniconda" : {
        "linux-64" : {
            // Separate entry for each python version,
            // so an appropriate installer can be used
            // based on the Python version used in the
            // version of FSL that is to be installed.
            "python3.10" : {
                "url"    : "https://repo.anaconda.com/miniconda/Miniconda3-py310_24.3.0-0-Linux-x86_64.sh",
                "sha256" : "303d65289f600fa4c85fe2b0fef9bb0848ec544a75a9c3079607d847918f7023",

                // Number of lines of output to expect
                // when running the miniconda installer
                // script - used to report progress to
                // the user.
                "output" : "194"
            },
            // as above
            "python3.11" : {},
            // as above
            "micromamba" : {}
        },
        // miniconda info for other platforms
        "macos-64" : {
            "python3.10" : {},
            "python3.11" : {},
            "micromamba" : {}
        },
        "macos-M1" : {
            "python3.10" : {},
            "python3.11" : {},
            "micromamba" : {}
        }
    },

    // The "versions" section contains information
    // about all available versions of FSL. FSL versions
    // are distributed as conda environment.yml files.
    "versions" : {

        // Reference to latest available
        // version - must be present
        "latest" : "6.2.0",

        // Builds available for FSL version 6.2.0
        "6.2.0"  : [
            {
                // Architecture/platform  - currently one of
                // "linux-64", "macos-64", or "macos-M1"
                "platform" : "linux-64",

                // Conda environment.yml file and SHA256 checksum
                "environment" : "https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/releases/fsl-6.1.0-linux-64.yml",
                "sha256"      : "952a0101fc19948e785572f8c06436c59f19c4ef92df9425f2e2033d4b986918",

                // Whether this environment may take advantage of
                // the conda-forge CUDA stack (e.g. pytorch). Note
                // that this is only relevant to conda-forge
                // packages that use CUDA, and is completely
                // unrelated to FSL CUDA tools such as eddy, ptx2,
                // etc, which are compiled so that they don't
                // require separate CUDA libraries.
                "cuda_enabled" : "false",

                // Dictionary containing installation details used
                // to report progress to the user during an
                // installation.
                //
                //   - The "install" field contains details for a
                //     standard installation.
                //   - Fields may also be present which contain
                //     details for "variant" installations -
                //     currently this is only used for
                //     environments that install the conda-forge
                //     CUDA stack.
                //
                // The format of the value of each field has
                // changed a few times - see the fslinstaller.py
                // script for more details.
                "output"        : {
                    "install"   : {
                        "version" : "4",
                        "value" : {
                            "bin"  : "1506",
                            "lib"  : "3218",
                            "pkgs" : "721",
                            "size" : "4063497239",
                        }
                    },
                }
                // Additional groups of packages that are installed
                // as separate child environments into $FSLDIR/envs/.
                // Each section has the same format as the build
                // entry for the main FSL environment, described above.
                "extras" : {
                    "truenet" : {
                        "environment"  : "https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/releases/fsl-6.1.0-linux-64-truenet.yml",
                        "sha256"       : "952a0101fc19948e785572f8c06436c59f19c4ef92df9425f2e2033d4b986918",
                        "cuda_enabled" : "true",
                        "output"       : {
                            // Expected output for regular installation
                            // (format as described above)
                            "install" : {},
                            // Expected output for installation with
                            // CUDA packages (format as described above)
                             "cuda"   : {}
                        }
                    }
                }
            },
            // Entries for other platforms
            {
                "platform"      : "macos-64",
                "environment"   : "...",
                "sha256"        : "...",
                "output"        : { "install"   : {}, },
                "extras"        : {}
            },
            {
                "platform"      : "macos-M1",
                "environment"   : "...",
                "sha256"        : "...",
                "output"        : { "install"   : {}, },
                "extras"        : {}
            }
        ],
        // Entries for older FSL versions
        "6.1.0" : [
            {
                "platform"      : "linux-64",
                "environment"   : "...",
                "sha256"        : "...",
                "output"        : { "install"   : {}, },
                "extras"        : {}
            },
            {
                "platform"      : "macos-64",
                "environment"   : "..",
                "sha256"        : "...",
                "output"        : { "install"   : {}, },
                "extras"        : {}
            },
            {
                "platform"      : "macos-M1",
                "environment"   : "...",
                "sha256"        : "...",
                "output"        : { "install"   : {}, },
                "extras"        : {}
            }
        ]
    }
}
